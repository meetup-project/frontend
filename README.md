## Зависимости

Требуется установить Node JS: https://nodejs.org/en

## Настройка проекта

```sh
npm install
```

### Запуск DEV окружения с hot-reload

```sh
npm run dev
```

Приложение будет доступно на http://localhost:5173/
