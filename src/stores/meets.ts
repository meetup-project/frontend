import type { MeetDetails, MeetItem } from '@/typings';
import {
  serverDeleteRequest,
  serverGetRequest,
  serverPostRequest,
  serverPutRequest
} from '@/utils/request';
import { defineStore } from 'pinia';
import { ref } from 'vue';

const URL = '/meets';

export const useMeetStore = defineStore('meets', () => {
  const list = ref<MeetItem[]>([]);
  const lastMeet = ref<MeetItem | null>(null);

  const getList = async () => {
    list.value = await serverGetRequest(URL);
  };

  const getItem: (id: string) => Promise<MeetDetails | null> = async (id) => {
    return await serverGetRequest(`${URL}/${id}/getFullInfo`);
  };

  const getLastStartedMeetIfExist = async () => {
    lastMeet.value = await serverGetRequest(`${URL}/lastStartedMeetIfExist`);
  };

  const createItem = async (data: unknown) => {
    await serverPostRequest(`${URL}`, JSON.stringify(data));
  };

  const updateItem = async (id: number, data: unknown) => {
    await serverPutRequest(`${URL}/${id}`, JSON.stringify(data));
  };

  const deleteItem = async (id: number) => {
    await serverDeleteRequest(`${URL}/${id}`);
    list.value.splice(
      list.value.findIndex((item) => item.id === id),
      1
    );
  };

  return {
    list,
    lastMeet,
    getList,
    getItem,
    getLastStartedMeetIfExist,
    createItem,
    updateItem,
    deleteItem
  };
});
