import type { SpeakerItem, SpeakerDetails } from '@/typings';
import {
  serverDeleteRequest,
  serverGetRequest,
  serverPostRequest,
  serverPutRequest
} from '@/utils/request';
import { defineStore } from 'pinia';
import { ref } from 'vue';

const URL = `/speakers`;

export const useSpeakerStore = defineStore(`speakers`, () => {
  const list = ref<SpeakerItem[]>([]);

  const getList = async () => {
    list.value = await serverGetRequest(`${URL}`);
  };

  const getItem: (id: string) => Promise<SpeakerDetails | null> = async (id) => {
    return await serverGetRequest(`${URL}/${id}/getFullInfo`);
  };

  const createItem = async (data: unknown) => {
    await serverPostRequest(`${URL}`, JSON.stringify(data));
  };

  const updateItem = async (id: number, data: unknown) => {
    await serverPutRequest(`${URL}/${id}`, JSON.stringify(data));
  };

  const deleteItem = async (id: number) => {
    await serverDeleteRequest(`${URL}/${id}`);
    list.value.splice(
      list.value.findIndex((item) => item.id === id),
      1
    );
  };

  return {
    list,
    getList,
    getItem,
    createItem,
    updateItem,
    deleteItem
  };
});
