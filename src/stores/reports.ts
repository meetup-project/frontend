import type { ReportDetails } from '@/typings';
import {
  serverDeleteRequest,
  serverGetRequest,
  serverPostRequest,
  serverPutRequest
} from '@/utils/request';
import { defineStore } from 'pinia';
import { ref } from 'vue';

const URL = `/reports`;

export const useReportStore = defineStore(`reports`, () => {
  const list = ref<ReportDetails[]>([]);

  const getList = async () => {
    list.value = await serverGetRequest(`${URL}`);
  };

  const getItem: (id: string) => Promise<ReportDetails | null> = async (id) => {
    return await serverGetRequest(`${URL}/${id}`);
  };

  const createItem = async (data: unknown) => {
    await serverPostRequest(`${URL}`, JSON.stringify(data));
  };

  const updateItem = async (id: number, data: unknown) => {
    await serverPutRequest(`${URL}/${id}`, JSON.stringify(data));
  };

  const deleteItem = async (id: number) => {
    await serverDeleteRequest(`${URL}/${id}`);
    list.value.splice(
      list.value.findIndex((item) => item.id === id),
      1
    );
  };

  return {
    list,
    getList,
    getItem,
    createItem,
    updateItem,
    deleteItem
  };
});
