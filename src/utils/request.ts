async function serverGetRequest(url: string) {
  const response = await fetch(`${import.meta.env.VITE_API_URL}${url}/`);
  if (response.ok) {
    return await response.json();
  } else {
    throw new Error(response.statusText);
  }
}

async function serverPostRequest(url: string, data: string) {
  const response = await fetch(`${import.meta.env.VITE_API_URL}${url}/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: data
  });

  if (!response.ok) {
    throw new Error('Произошла ошибка');
  }
}

async function serverPutRequest(url: string, data: string) {
  const response = await fetch(`${import.meta.env.VITE_API_URL}${url}/`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: data
  });

  if (!response.ok) {
    throw new Error('Произошла ошибка');
  }
}

async function serverDeleteRequest(url: string) {
  const response = await fetch(`${import.meta.env.VITE_API_URL}${url}/`, {
    method: 'DELETE'
  });

  if (!response.ok) {
    throw new Error('Произошла ошибка');
  }
}

export { serverGetRequest, serverPostRequest, serverPutRequest, serverDeleteRequest };
