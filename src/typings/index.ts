interface MeetShort {
  id: number;
  title: string;
}

interface MeetItem extends MeetShort {
  description: string;
  date: string;
  broadcastLink: string;
  broadcastLogin: string;
  broadcastPassword: string;
  countListener: number;
  registrationForm: string;
}

interface ReportItem {
  id: number;
  title: string;
  description: string;
  meetup: MeetShort | null;
  speaker: SpeakerItem | null;
  order: number;
  presentationLink: string;
  videoLink: string;
}

interface ReportDetails {
  id: number;
  title: string;
  description: string;
  meetup: MeetShort | null;
  speaker: SpeakerItem | null;
  order: number;
  presentationLink: string;
  videoLink: string;
}

interface SpeakerItem {
  id: number;
  lastName: string;
  firstName: string;
  surname: string;
  photo: string;
  speakerInfo: string;
  fullName: string;
}

interface MeetDetails extends MeetItem {
  reports: ReportDetails[] | null;
}

interface SpeakerDetails extends SpeakerItem {
  reports: ReportDetails[] | null;
}

export type { MeetItem, MeetDetails, ReportDetails, ReportItem, SpeakerItem, SpeakerDetails };
