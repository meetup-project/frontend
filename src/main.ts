import './assets/main.css';

import { createApp } from 'vue';
import { createPinia } from 'pinia';

import App from './App.vue';
import router from './router';

import { setLocale } from 'yup';

setLocale({
  // use constant translation keys for messages without values
  mixed: {
    required: 'Это поле обязательно!'
  }
});

const app = createApp(App);

app.use(createPinia());
app.use(router);

app.mount('#app');
