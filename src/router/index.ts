import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/meets',
      children: [
        {
          path: '',
          name: 'meets',
          component: () => import('@/views/meets/ListView.vue')
        },
        {
          path: '/meets/:id',
          name: 'meets-details',
          component: () => import('@/views/meets/DetailsView.vue'),
          props: true
        }
      ]
    },
    {
      path: '/reports',
      children: [
        {
          path: '',
          name: 'reports',
          component: () => import('@/views/reports/ListView.vue')
        },
        {
          path: '/reports/:id',
          name: 'reports-details',
          component: () => import('@/views/reports/DetailsView.vue'),
          props: true
        }
      ]
    },
    {
      path: '/speakers',
      children: [
        {
          path: '',
          name: 'speakers',
          component: () => import('@/views/speakers/ListView.vue')
        },
        {
          path: '/speakers/:id',
          name: 'speakers-details',
          component: () => import('@/views/speakers/DetailsView.vue'),
          props: true
        }
      ]
    },
    {
      path: '/admin',
      component: () => import('@/layouts/AdminLayout.vue'),
      children: [
        {
          path: '',
          name: 'admin',
          component: () => import('@/views/admin/AdminView.vue')
        },
        {
          path: 'meets',
          children: [
            {
              path: 'create',
              name: 'meets-create',
              component: () => import('@/views/admin/meets/CreateMeet.vue')
            },
            {
              path: 'update',
              name: 'meets-update',
              component: () => import('@/views/admin/meets/UpdateMeet.vue')
            },
            {
              path: 'delete',
              name: 'meets-delete',
              component: () => import('@/views/admin/meets/DeleteMeet.vue')
            }
          ]
        },
        {
          path: 'reports',
          children: [
            {
              path: 'create',
              name: 'reports-create',
              component: () => import('@/views/admin/reports/CreateReport.vue')
            },
            {
              path: 'update',
              name: 'reports-update',
              component: () => import('@/views/admin/reports/UpdateReport.vue')
            },
            {
              path: 'delete',
              name: 'reports-delete',
              component: () => import('@/views/admin/reports/DeleteReport.vue')
            }
          ]
        },
        {
          path: 'speakers',
          children: [
            {
              path: 'create',
              name: 'speakers-create',
              component: () => import('@/views/admin/speakers/CreateSpeaker.vue')
            },
            {
              path: 'update',
              name: 'speakers-update',
              component: () => import('@/views/admin/speakers/UpdateSpeaker.vue')
            },
            {
              path: 'delete',
              name: 'speakers-delete',
              component: () => import('@/views/admin/speakers/DeleteSpeaker.vue')
            }
          ]
        }
      ]
    }
  ]
});

export default router;
